$(document).ready(function(){

    $(".video-youtube").fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none'
    });

    if( $('.header-slider').length > 0){
        $('.header-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            speed: 1000,
            fade: true,
            cssEase: 'linear'
        });
    }

    if( $('.banner-slider').length > 0){
        $('.banner-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            speed: 1000,
            dots: true
        });
    }

    if( $('.brand-slider').length > 0){
        $('.brand-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            speed: 1000,
            slidesToShow: 15,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 15,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 8,
                        slidesToScroll: 8
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                        arrows: false
                    }
                }
            ]
        });
    }

    if( $('.products-slider').length > 0){
        $('.products-slider').slick({
            // autoplay: true,
            // autoplaySpeed: 2000,
            infinite: true,
            speed: 1000,
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                }
            ]
        });
    }

    if( $('.service-slider').length > 0){
        $('.service-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            speed: 1000,
            slidesToShow: 8,
            slidesToScroll: 8,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        arrows: false
                    }
                }
            ]
        });
    }

    if( $('.slider-for').length > 0){
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            // dots: true,
            // centerMode: true,
            focusOnSelect: true
        });
    }

    // $(window).scroll(function() {
    //     var h_headerTop = $('.header-top').height();
    //     var h_headerMain = $('.navbar-epal').outerHeight( true );
    //     if ($(this).scrollTop() > h_headerTop && $(this).width() >= 768){
    //         $('.navbar-epal').addClass("sticky");
    //     }
    //     else{
    //         $('.navbar-epal').removeClass("sticky");
    //     }
    //     $('.header-main').css({"height" : h_headerMain});
    // });
    //
    // $('#navbar-scroll').find('a').click(function () {
    //     var elementID = $(this).attr('href');
    //     $element = $(elementID);
    //     if($element.length > 0){
    //         $('html,body').animate({scrollTop: $element.offset().top});
    //     }
    // });


    // if( $('.header-slider').length > 0){
    //     $('.header-slider').slick({
    //         autoplay: true,
    //         autoplaySpeed: 2000,
    //         infinite: true,
    //         speed: 1000,
    //         fade: true,
    //         cssEase: 'linear'
    //     });
    // }
    //
    // if( $('.banner-slider').length > 0){
    //     $('.banner-slider').slick({
    //         autoplay: true,
    //         autoplaySpeed: 2000,
    //         infinite: true,
    //         speed: 1000,
    //         dots: true
    //     });
    // }
    //
    // if( $('.brand-slider').length > 0){
    //     $('.brand-slider').slick({
    //         autoplay: true,
    //         autoplaySpeed: 2000,
    //         infinite: true,
    //         speed: 1000,
    //         slidesToShow: 10,
    //         slidesToScroll: 1
    //     });
    // }
    //
    // if( $('.service-slider').length > 0){
    //     $('.service-slider').slick({
    //         autoplay: true,
    //         autoplaySpeed: 2000,
    //         infinite: true,
    //         speed: 1000,
    //         slidesToShow: 7,
    //         slidesToScroll: 1
    //     });
    // }
    //
    // if( $('.slider05').length > 0){
    //     $('.slider05').slick({
    //         // autoplay: true,
    //         // autoplaySpeed: 2000,
    //         infinite: true,
    //         speed: 1000,
    //         slidesToShow: 5,
    //         slidesToScroll: 1
    //     });
    // }
    //
    // if( $('.slider-for').length > 0){
    //     $('.slider-for').slick({
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         arrows: false,
    //         fade: true,
    //         asNavFor: '.slider-nav'
    //     });
    //     $('.slider-nav').slick({
    //         slidesToShow: 5,
    //         slidesToScroll: 1,
    //         asNavFor: '.slider-for',
    //         // dots: true,
    //         // centerMode: true,
    //         focusOnSelect: true
    //     });
    // }


    // $(".various").fancybox({
    //     maxWidth	: 800,
    //     maxHeight	: 800,
    //     fitToView	: false,
    //     width		: '85%',
    //     height		: '85%',
    //     autoSize	: false,
    //     closeClick	: false,
    //     openEffect	: 'none',
    //     closeEffect	: 'none'
    // });

});